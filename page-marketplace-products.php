<?php
/* Template Name: Marketplace :: Products */

get_header(); 

global $kiwi_theme_option; ?>

<div class="container marketplace mp-vc-items"<?php esc_attr( kiwi_rtl() ); ?>>
	<div class="row" role="main">	
		
		<div class="container">
				
		<?php //if(have_posts()) : while(have_posts()) : the_post(); ?>				
			<?php //the_content(); ?>	
		<?php //endwhile; endif; ?>
                
                <?php ////////////////////////////////////////////////////// ?>    
                    
                <?php
                $args = array(
                    'fields'            => 'ids',
                    'post_type'         => 'download',
                    'catid'             => '209',
                    'no_found_rows'     => true,
                    'posts_per_page'    => '12',
                    'orderby'           => 'date',
                    'order'             => 'DESC',
                    'post_status'       => 'published',
                );

                $downloads = get_posts( $args );
                foreach ( $downloads as $key => $download_id ) {
                    $download = new EDD_Download( $download_id );
                    var_dump($download);                wp_die();
                }
                
                ?>
                    
                <?php ////////////////////////////////////////////////////// ?>
                    
                <?php $the_query = new WP_Query( 'posts_per_page=12&category__in=209'); ?>
                <?php while ($the_query -> have_posts()) : $the_query -> the_post(); ?>
                    <?php echo 'test'; ?>
                    <div class="vc-post-items">
                        <!--<span class="tags">کودک درون, آرامش ذهن, هدف</span>-->
                        <!--<span class="pull-right"><i class="fa fa-download"></i> <span>1</span></span>-->
                        <div class="truncate no">
                            <h4>
                                <a href="<?php the_permalink(); ?>">
                                   <?php the_title(); ?>
                                </a>
                            </h4>   
                        </div>
                        <div class="excerpt">
                            <?php the_excerpt(); ?>
                            <?php
                            $post_format = get_post_format($post->ID);
                            if($post_format == 'audio'){
                                $fallback = wp_mediaelement_fallback( 
                                    esc_url( get_post_meta( get_the_ID(),
                                            '_cmb2_audiofile', true )));
                                if(!empty( $fallback )) {
                                    $attr = apply_filters( 'mp_hook_audio_settings', array(
                                        'src'      => strip_tags($fallback),
                                    ));
                                    ?>
                                    <div class="audio-player" itemprop="audio"><?php echo wp_audio_shortcode( $attr );?></div><br>
                                <?php }
                            } else if($post_format != 'video'){
                                ?>
                                <div class="more-btn"><a href="<?php the_permalink(); ?>" class="more-link" title="ادامه مطلب">ادامه مطلب</a></div>
                            <?php } ?>
                        </div>
                        <div class="clear"></div>
                    </div>
                <?php 
                endwhile;
                wp_reset_postdata();
                ?>
		
&nbsp;
			</div>
	</div>
</div>			

<?php get_footer(); ?>

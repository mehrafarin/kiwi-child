<?php
/*
Template Name: GravityForms Payment status Moshavere
*/

get_header();

// retrieve query strings
$retry_url = isset($_REQUEST['retry_url']) && !empty($_REQUEST['retry_url']) ? $_REQUEST['retry_url']:get_option('page_on_front');
if(isset($_REQUEST['first_name']) && !empty($_REQUEST['first_name'])
&& isset($_REQUEST['transaction_id']) && !empty($_REQUEST['transaction_id'])
&& isset($_REQUEST['payment_status']) && !empty($_REQUEST['payment_status'])
&& isset($_REQUEST['entry_id']) && !empty($_REQUEST['entry_id'])){
	$first_name = htmlspecialchars($_REQUEST['first_name']);
	$transaction_id = htmlspecialchars($_REQUEST['transaction_id']);
	$payment_status = htmlspecialchars($_REQUEST['payment_status']);
	$entry_id = htmlspecialchars($_REQUEST['entry_id']);
	if($payment_status == 'Paid' || $payment_status == 'موفق'){
		$template = '[vc_row][vc_column width="1/6"][/vc_column][vc_column width="2/3"][vc_message message_box_color="success" icon_fontawesome="fa fa-check" css_animation="flipInX"]</p>
<p style="text-align: center;"><span style="font-size: 12pt; color: #993366;">'.$first_name.' عزیز پرداخت شما با موفقیت انجام شد.</span></p>
<p style="text-align: center;"><span style="font-size: 12pt; color: #993366;">شماره تراکنش: '.$transaction_id.'        شماره فاکتور: '.$entry_id.'</span></p>
<p>[/vc_message][vc_message message_box_color="green" icon_type="pixelicons" css_animation="flipInX" icon_pixelicons="vc_pixel_icon vc_pixel_icon-info"]</p>
<p style="text-align: center;"><span style="font-size: 12pt; color: #993366;">پیام استاد:</span></p>
<p style="text-align: center;"><span style="font-size: 12pt; color: #993366;">از شما دوست عزیز متشکرم و امیدوارم براتون پر برکت باشه.</span></p>
<p style="text-align: center;"><span style="font-size: 12pt; color: #993366;">به لطف خداوند 10 برابرش به حسابتون بیاد.</span></p>
<p style="text-align: center;"><span style="font-size: 12pt; color: #993366;">«اعظم صادقیان»</span></p>
<p>[/vc_message][vc_btn title="بازگشت به صفحه اصلی" color="danger" align="center" i_align="right" i_icon_fontawesome="fa fa-home" css_animation="none" add_icon="true" link="url:%2F|||"][/vc_column][vc_column width="1/6"][/vc_column][/vc_row]';
	} else{
		$template = '[vc_row][vc_column width="1/6"][/vc_column][vc_column width="2/3"][vc_message message_box_color="danger" icon_fontawesome="fa fa-times" css_animation="flipInX"]</p>
<p style="text-align: center;"><span style="font-size: 12pt; color: #993366;">متاسفانه پرداخت شما ناموفق بود.</span></p>
<p style="text-align: center;"><span style="font-size: 12pt; color: #993366;">شماره تراکنش: '.$transaction_id.'        شماره فاکتور: '.$entry_id.'</span></p>
<p style="text-align: center;"><span style="font-size: 12pt; color: #993366;"> درصورتی که اشکالی در روند پرداخت دارید، شماره تراکنش و شماره فاکتور را یادداشت نموده و از طریق بخش تماس با ما، با مدیر سایت موضوع را مطرح نمایید و شماره تراکنش و شماره فاکتور را نیز ارسال نمایید.</span></p>
<p>[/vc_message][vc_row_inner][vc_column_inner width="1/3"][vc_btn title="بازگشت به صفحه اصلی" color="danger" align="center" i_align="right" i_icon_fontawesome="fa fa-home" css_animation="none" add_icon="true" link="url:%2F|||"][/vc_column_inner][vc_column_inner width="1/3"][vc_btn title="تماس با ما" color="danger" align="center" i_align="right" i_icon_fontawesome="fa fa-phone-square" css_animation="none" add_icon="true" link="url:%2F%3Fp%3D427||target:%20_blank|"][/vc_column_inner][vc_column_inner width="1/3"][vc_btn title="تلاش مجدد" color="danger" align="center" i_align="right" i_icon_fontawesome="fa fa-refresh" css_animation="none" add_icon="true" link="url:%2F%3Fp%3D'.$retry_url.'|||"][/vc_column_inner][/vc_row_inner][/vc_column][vc_column width="1/6"][/vc_column][/vc_row]';
	}
}
else{
	$template = '[vc_row][vc_column width="1/6"][/vc_column][vc_column width="2/3"][vc_message message_box_color="warning" icon_fontawesome="fa fa-exclamation-triangle" css_animation="flipInX"]</p>
<p style="text-align: center;"><span style="font-size: 12pt; color: #993366;">خطایی رخ داده است، لطفا از طریق بخش تماس با ما، موضوع را با مدیر سایت در میان بگذارید.</span></p>
<p>[/vc_message][vc_row_inner][vc_column_inner width="1/3"][vc_btn title="بازگشت به صفحه اصلی" color="danger" align="center" i_align="right" i_icon_fontawesome="fa fa-home" css_animation="none" add_icon="true" link="url:%2F|||"][/vc_column_inner][vc_column_inner width="1/3"][vc_btn title="تماس با ما" color="danger" align="center" i_align="right" i_icon_fontawesome="fa fa-phone-square" css_animation="none" add_icon="true" link="url:%2F%3Fp%3D427||target:%20_blank|"][/vc_column_inner][vc_column_inner width="1/3"][vc_btn title="تلاش مجدد" color="danger" align="center" i_align="right" i_icon_fontawesome="fa fa-refresh" css_animation="none" add_icon="true" link="url:%2F%3Fp%3D'.$retry_url.'|||"][/vc_column_inner][/vc_row_inner][/vc_column][vc_column width="1/6"][/vc_column][/vc_row]';
}

?>
<div class="container" id="payment-status-template" itemscope itemtype="http://schema.org/Blog">
	<div class="row" role="main">
		<div class="col-sm-12 col-md-12 fullwidth">
		<?php echo do_shortcode($template);?>
		</div>
	</div>
</div>

<?php get_footer(); ?>

<?php
function kiwi_child_enqueue_styles() {

    $parent_style = 'Kiwi';

    wp_enqueue_style( $parent_style, get_template_directory_uri() . '/style.css' );
    wp_enqueue_style( 'Kiwi-child',
        get_stylesheet_directory_uri() . '/style.css',
        array( $parent_style ),
        wp_get_theme()->get('Version')
    );
}
add_action( 'wp_enqueue_scripts', 'kiwi_child_enqueue_styles' );

/*
function edd_ck_show_file_sizes( $post_id ) {
	$files = edd_get_download_files( $post_id, null );
	$decimals = 2;
	$sz = 'BKMGTP';
	$header = _n( 'File Size', 'File Sizes', count( $files ), 'edd' );
	echo '<h5>' . $header . '</h5>';
	echo '<ul>';
	foreach( $files as $file ) {
		$bytes = filesize( get_attached_file( $file['attachment_id'] ) );
		$factor = floor((strlen($bytes) - 1) / 3);
		echo '<li>' . $file['name'] . ' - ' . sprintf( "%.{$decimals}f", $bytes / pow( 1024, $factor) ) . @$sz[$factor] . '</li>';
	}
	echo '</ul>';
}
add_action( 'edd_after_download_content', 'edd_ck_show_file_sizes', 10, 1 );
*/


// dyname EDD purchase button for access level and ux
function takexpert_dynamic_edd_purchase_button(){
    // if user is logged in
    if(is_user_logged_in()){
        if( edd_has_user_purchased(get_current_user_id()
, get_the_ID()) ) {
	        ?>
          	  <!-- the current user HAS purchased product -->
	            <div class="edd_purchase_button">
			             <a class="button takexpert-dynamic-button-downloaded"
                    href="/%d9%be%db%8c%d8%b4%d8%ae%d9%88%d8%a7%d9%86/?task=download"
                    class="button takexpert-dynamic-button-downloaded">دریافت محصول</a>
              </div>

	            <!-- the current user has purchased but want to purchase another prodcut -->
              <div class="edd_purchase_button">
                <?php echo edd_get_purchase_link( array(
                  'price' => '0',
                  'class' => 'addcart',
                  'download_id' => get_the_ID(),
                  'text' => 'خرید مجدد' ) );?></div>
	            <?php
        } else {
              ?>
	            <!-- the current user has NOT purchase product -->
              <div class="edd_purchase_button">
                <?php echo edd_get_purchase_link(array(
                    'price' => '0',
                    'class' => 'addcart',
                    'download_id' => get_the_ID()) );?></div>
            <?php
        }

	}
	else{
	    $item_url = get_permalink(get_the_ID());
      $upme_options = get_option('upme_options');
      $login_url = get_permalink($upme_options['login_page_id']);
      $registeration_url = get_permalink($upme_options['registration_page_id']);
      $help_url = get_permalink(8623);
      $help__registration_url = get_permalink(24011);
      $help_pay_url = get_permalink(24013);
      // $help_dynamic_password = get_permalink(24470);
    ?>
    <div class="edd_purchase_submit_wrapper">
      <div class="takexpert-dynamic-button-caution">برای خرید محصولات لازم است به حساب خود وارد شوید.<br>چنانچه قبلا در سایت عضو نشده اید، ابتدا باید عضو شوید
      </div>
    </div>
    <div class="help col-md-12">
      <a class="faq button takexpert-dynamic-button"
        href="<?php echo $help_pay_url;?>" target="_blank">راهنمای خرید</a>
      <!-- <a class="faq button takexpert-dynamic-button"
        href="<?php //echo $help_dynamic_password;?>" target="_blank">دریافت رمز پویا</a> -->
      <a class="faq button takexpert-dynamic-button"
        href="<?php echo $help__registration_url;?>" target="_blank">راهنمای عضویت</a>
    </div>
    <div class="auth col-md-12">
    <a class="button takexpert-dynamic-button"
       href="<?php echo $login_url;?>">ورود</a>
    <a class="button takexpert-dynamic-button"
       href="<?php echo $registeration_url;?>">عضویت</a>
    </div>

    <?php
	}
}
add_action( 'takexpert_dynamic_edd_purchase_button', 'takexpert_dynamic_edd_purchase_button');

// auto redirect for login to previous url dynamicly
function takexpert_upme_login_auto_redirect(){
    $current_url = $_SERVER["HTTP_REFERER"];
    $upme_options = get_option('upme_options');
    $upme_blacklist_links = array(trim(get_permalink($upme_options['login_page_id'])), trim(get_permalink($upme_options['reset_password_page_id'])), trim(get_permalink($upme_options['registration_page_id'])));
    if(in_array(trim($_SERVER["HTTP_REFERER"]), $upme_blacklist_links)){
      $current_url = site_url();
    }
    return do_shortcode("[upme_login redirect_to=\"$current_url\"]");
}
add_shortcode( 'takexpert_upme_login_auto_redirect', 'takexpert_upme_login_auto_redirect' );

// auto redirect for register to previous url dynamicly
function takexpert_upme_register_auto_redirect(){
    $current_url = $_SERVER["HTTP_REFERER"];
    $upme_options = get_option('upme_options');
    $upme_blacklist_links = array(trim(get_permalink($upme_options['login_page_id'])), trim(get_permalink($upme_options['reset_password_page_id'])), trim(get_permalink($upme_options['registration_page_id'])));
    if(in_array(trim($_SERVER["HTTP_REFERER"]), $upme_blacklist_links)){
      $current_url = site_url();
    }
    return do_shortcode("[upme_registration redirect_to=\"$current_url\"]");
}
add_shortcode( 'takexpert_upme_register_auto_redirect', 'takexpert_upme_register_auto_redirect' );

// remove website field from comments
add_filter('comment_form_default_fields', 'website_remove');
function website_remove($fields){
    if(isset($fields['url']))
    unset($fields['url']);
    return $fields;
}

// required login function
function takexpert_required_login(){
  $upme_options = get_option('upme_options');
  $login_url = get_permalink($upme_options['login_page_id']);
  $registeration_url = get_permalink($upme_options['registration_page_id']);
  $help_url = get_permalink(8623);
  ?>
  <div class="required-login">
    <div class="content">
      <p>برای استفاده از این بخش به سایت وارد شوید
                    <br>چناچه قبلا عضو نشده اید، ابتدا در سایت عضو شوید</p>
    </div>
    <br>
    <div class="help-buttons">
      <a class="button takexpert-dynamic-button" href="<?=$login_url?>">ورود</a>
      <a class="button takexpert-dynamic-button" href="<?=$registeration_url?>">عضویت</a>
      <a class="faq button takexpert-dynamic-button" href="<?=$help_url?>" target="_blank">راهنمای عضویت</a>
    </div>
  </div>
  <?php
}

// show add new post and list posts for frontend user
function takexpert_upme_frontend_user_box(){
    if(is_user_logged_in()){
    	global $upfp;
    	$params = array(
    		'id' => get_current_user_id(),
    		'view' => null,
    		'hide_profile_tabs' => false,
    		'takexpert_show_success_panel' => 1
    	);
        ?>
    	<div class="wpb_raw_code wpb_content_element wpb_raw_html">
            <div class="wpb_wrapper">
                <?php echo $upfp->publisher->profile_view_forms("", $params);?>
            </div>
        </div>
        <?php
    }
    else{
        takexpert_required_login();
    }
}
add_shortcode( 'takexpert_upme_frontend_user_box', 'takexpert_upme_frontend_user_box' );

function takexpert_consultation_request(){
    if(is_user_logged_in()){
        /*
    	echo do_shortcode('[vc_row][vc_column width="1/3"][vc_custom_heading text="گام اول" font_container="tag:h1|font_size:36px|text_align:center|color:%231e73be|line_height:20px" css_animation="appear"][vc_separator color="violet" style="shadow" border_width="2" css_animation="flipInY"][vc_column_text]

در گام اول، ابتدا لازم است حق مشاوره را پرداخت نمایید

هر جلسه مشاوره معادل 45 دقیقه می باشد.
[purchase_link id="11377" text="خرید محصول" style="button" color="blue"]

[/vc_column_text][/vc_column][vc_column width="1/3"][vc_custom_heading text="گام دوم" font_container="tag:h1|font_size:36px|text_align:center|color:%231e73be|line_height:20px" css_animation="appear"][vc_separator color="violet" style="shadow" border_width="2" css_animation="flipInY"][vc_column_text]پس از پرداخت موفق، فاکتور پرداخت به شما نشان داده خواهد شد.

شماره پرداخت را یادداشت و در فرم گام سوم وارد نمایید[/vc_column_text][/vc_column][vc_column width="1/3"][vc_custom_heading text="گام سوم" font_container="tag:h1|font_size:36px|text_align:center|color:%231e73be|line_height:20px" css_animation="appear"][vc_separator color="violet" style="shadow" border_width="2" css_animation="flipInY"][contact-form-7 id="8801"][/vc_column][/vc_row]');
        */
        echo '<style> .vc_message_box-icon {display: none;} .vc_message_box ul {margin: 20px !important;} </style>';
        echo do_shortcode('[vc_row][vc_column][vc_message message_box_color="violet" icon_type="pixelicons" icon_pixelicons="vc_pixel_icon vc_pixel_icon-info"]
<ul style="list-style-type: circle;">
 	<li style="text-align: right;"><span style="font-size: 12pt;">با استفاده از دکمه سبز رنگ زیر می توانید فرآیند پرداخت هزینه مشاوره را دنبال کنید.</span></li>
 	<li style="text-align: right;"><span style="font-size: 12pt;">پس از پرداخت هزینه،  شماره پرداخت از طریق پیامک به شما اعلام می گردد، در حفظ و نگهداری آن کوشا باشید.</span></li>
 	<li style="text-align: right;"><span style="font-size: 12pt;">پس از پرداخت، از 24 الی 72 ساعت بابت زمان مراجعه با شما هماهنگ خواهد شد.</span></li>
 	<li style="text-align: right;"><span style="font-size: 12pt;">پس از مراجعه شماره پرداخت را به مشاور تحویل دهید.</span></li>
</ul>
<hr />
<span style="font-size: 12pt;">[purchase_link id="11377" text="" style="button" color="blue"]</span>[/vc_message][/vc_column][/vc_row]');
    }
    else{
        takexpert_required_login();
    }
}
add_shortcode( 'takexpert_consultation_request', 'takexpert_consultation_request' );

function takexpert_recent_posts($atts){
    global $post;
    $atts = shortcode_atts( array(
        'count' => '5',
        'catid' => '298',
        'header_text' => '',
        'all_link' => '',
        'all_text' => '',
    ), $atts, 'takexpert_recent_posts' );
    ?>
    <?php if(!empty($atts['header_text'])): ?>
        <div class="edd_widget_title_vc">
        <h3><?php echo $atts['header_text']; ?></h3>
        </div>
    <?php endif; ?>
    <div class="recent-success">
    <?php $the_query = new WP_Query( 'posts_per_page='.$atts['count'].'&category__in='.$atts['catid'] ); ?>
    <?php while ($the_query -> have_posts()) : $the_query -> the_post(); ?>
        <div class="vc-post-items">
            <!--<span class="tags"></span>-->
            <!--<span class="pull-right"><i class="fa fa-download"></i><span>1</span></span>-->
            <div class="truncate no">
                <h4>
                    <a href="<?php the_permalink(); ?>">
                       <?php the_title(); ?>
                    </a>
                </h4>
            </div>
			<?php
				$post_format = get_post_format($post->ID);
				if($post_format == 'audio' || $post_format == 'video'){
					$excerpt_class = 'excerpt notext';
				} else {
					$excerpt_class = 'excerpt text';
				}
			?>
            <div class="<?php echo $excerpt_class;?>">
                <?php the_excerpt(); ?>
			            </div>

                <?php
                //$post_format = get_post_format($post->ID);
                if($post_format == 'audio'){
                    $fallback = wp_mediaelement_fallback(
                        esc_url( get_post_meta( get_the_ID(),
                                '_cmb2_audiofile', true )));
                    if(!empty( $fallback )) {
                        $attr = apply_filters( 'mp_hook_audio_settings', array(
                            'src'      => strip_tags($fallback),
                        ));
                        ?>
                        <div class="audio-player" itemprop="audio"><?php echo wp_audio_shortcode( $attr );?></div><br>
                    <?php }
                } else if($post_format != 'video'){
                    ?>
                    <div class="more-btn"><a href="<?php the_permalink(); ?>" class="more-link" title="ادامه مطلب">ادامه مطلب</a></div>
                <?php } ?>
            <div class="clear"></div>
        </div>
    <?php
    endwhile;
    wp_reset_postdata();
    ?>
    </div>
    <?php if(!empty($atts['all_link']) && !empty($atts['all_text'])): ?>
        <div class="mp-vc-readmore"><a href="<?php echo $atts['all_link'];?>"><?php echo $atts['all_text'];?></a></div>
    <?php endif; ?>
    <?php
}
add_shortcode( 'takexpert_recent_posts', 'takexpert_recent_posts' );

// change excerpt more text
function takexpert_new_excerpt_more() {
  remove_filter('excerpt_more', 'new_excerpt_more');
  return ' ...';
}
add_filter('excerpt_more','takexpert_new_excerpt_more',11);

// limit excerpt characters
function takexpert_limit_excerpt( $excerpt ) {
    $character_limit = 200;
    $excerpt = strip_tags($excerpt);
    if(strlen($excerpt) > $character_limit){
        $excerpt = substr($excerpt, 0, $character_limit);
        $excerpt = substr($excerpt, 0, strripos($excerpt, " "));
        $excerpt = trim(preg_replace( '/\s+/', ' ', $excerpt));
        $excerpt = '<p>'.$excerpt.' ...</p>';
    }
    else {
        $excerpt = preg_replace('/\[(.*)\]/', '', $excerpt);
        $excerpt = '<p>'.$excerpt.'</p>';
    }
    return $excerpt;
}
add_filter('get_the_excerpt', 'takexpert_limit_excerpt', 9999);

function takexpert_custom_excerpt_length( $length ) {
    return 200;
}
add_filter( 'excerpt_length', 'takexpert_custom_excerpt_length', 9999 );



/*===============================================================
	Custom Pagination
===============================================================*/
function kiwi_content_nav() {
    global $wp_query;
    $big = 999999999;
    $pagination = paginate_links( array(
        'base'              => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
        'format'            => '?paged=%#%',
        'current'           => max( 1, get_query_var('paged') ),
        'total'             => $wp_query->max_num_pages,
        'prev_next'         => True,
        'prev_text'         => esc_html__('صفحه قبل','kiwi'),
        'next_text'         => esc_html__('صفحه بعد','kiwi'),
    ) );
    if(!empty($pagination)){
        $tag = '<div class="kiwi-pagination">' . PHP_EOL;
        $tag .= '<span class="pagination-title">صفحه بندی</span>'. PHP_EOL;
        $tag .= $pagination . PHP_EOL;
        $tag .= '</div>' . PHP_EOL;
        echo $tag;
    }
}


/*
function wp_audio_shortcode_dowload_link( $html, $atts, $audio, $post_id, $library ) {
    $html .='<br><a href="'.$atts['src'].'">دانلود</a>';
    return $html;

}
add_filter( 'wp_audio_shortcode','wp_audio_shortcode_dowload_link',5,10);
*/

// set edd_email as not required field in edd checkout page
function takexpert_edd_required_checkout_fields( $required_fields ) {
    unset($required_fields['edd_email']);
    return $required_fields;
}
add_filter( 'edd_purchase_form_required_fields', 'takexpert_edd_required_checkout_fields' );


/* Plugin Name: First name plus last name as default display name. */
function takexpert_set_display_name_as_firstname( $user_id ){
   $data = get_userdata( $user_id );
   // check if these data are available in your real code!
   wp_update_user(
       array (
           'ID' => $user_id,
           'display_name' => "$data->first_name",
           'nickname' => "$data->first_name"
       )
   );
}
add_action( 'user_register', 'takexpert_set_display_name_as_firstname' );

/* custom edd discount design */
function takexpert_custom_edd_discount_template(){
    // define controller variables
  $edd_points_available = FALSE;
  // $edd_discount_available = FALSE;
  $edd_discount_available = TRUE;

  if( isset( $_GET['payment-mode'] ) && edd_is_ajax_disabled() ) {
		return; // Only show before a payment method has been selected if ajax is disabled
	}

	if( ! edd_is_checkout() ) {
		return;
	}

  // check if edd points available
  if(array_key_exists('edd_points_render', $GLOBALS)){
    $edd_points_available = TRUE;
  }

  // check if edd discount available
  /*
  if ( edd_has_active_discounts() && edd_get_cart_total() ){
    $edd_discount_available = TRUE;
  }
  */

  if($edd_discount_available && $edd_points_available):
  ?>
      <div class="discounts-tabs">
  <?php
  endif;

  // print edd points template
  if( $edd_points_available ){
    global $edd_points_render;
    $edd_points_render->edd_points_redeem_point_markup();
  }

  // print edd discount template
	if ( $edd_discount_available ) :

		$color = edd_get_option( 'checkout_color', 'blue' );
		$color = ( $color == 'inherit' ) ? '' : $color;
		$style = edd_get_option( 'button_style', 'button' );
?>

    <fieldset id="edd_discount_code">
      <p id="edd-discount-code-wrap-custom" class="edd-cart-adjustment" style="display: block !important;">
        <label class="edd-label" for="edd-discount">
          <?php _e( 'Discount', 'easy-digital-downloads' ); ?>
        </label>
        <span class="edd-description"><?php _e( 'Enter a coupon code if you have one.', 'easy-digital-downloads' ); ?></span>
        <span class="edd-discount-code-field-wrap">
          <input class="edd-input" type="text" id="edd-discount" name="edd-discount" placeholder="<?php _e( 'Enter discount', 'easy-digital-downloads' ); ?>"/>
          <input type="submit" class="edd-apply-discount edd-submit button <?php echo $color . ' ' . $style; ?>" value="<?php echo _x( 'Apply', 'Apply discount at checkout', 'easy-digital-downloads' ); ?>"/>
        </span>
        <span class="edd-discount-loader edd-loading" id="edd-discount-loader" style="display:none;"></span>
        <span id="edd-discount-error-wrap" class="edd_error edd-alert edd-alert-error" aria-hidden="true" style="display:none;"></span>
      </p>
    </fieldset>
  <?php
	endif;
  if ( $edd_points_available && $edd_discount_available ) :
  ?>
    </div>
  <?php
  endif;
}
add_action( 'edd_before_purchase_form', 'takexpert_custom_edd_discount_template', -2 );

// disable old edd discount template
remove_action( 'edd_checkout_form_top', 'edd_discount_field', -1);

// disable old edd points discount action
if(array_key_exists('edd_points_render', $GLOBALS)){
  global $edd_points_render;
  remove_action( 'edd_before_purchase_form', array( $edd_points_render,'edd_points_redeem_point_markup' ) );
}

// customize edd checkout page submit button
add_filter('edd_checkout_button_purchase', 'takexpert_custom_checkout_button_purchase');
function takexpert_custom_checkout_button_purchase($fields){
  $color = edd_get_option( 'checkout_color', 'blue' );
  $color = ( $color == 'inherit' ) ? '' : $color;
  $style = edd_get_option( 'button_style', 'button' );

  ob_start();
  ?>
    <input type="submit" class="edd-submit <?php echo $color; ?> <?php echo $style; ?>" id="edd-purchase-button" name="edd-purchase" value="<?php _e('Payment', 'easy-digital-downloads'); ?>"/>
  <?php
    return ob_get_clean();
}

add_filter( 'edd_get_cart_discount_html', 'takexpert_custom_checkout_applied_discount_template', 999, 4);
function takexpert_custom_checkout_applied_discount_template($discount_html, $discount, $rate, $remove_url){
  ob_start();
  ?>
  <tr class="edd_cart_footer_row edd_cart_discount_row" id="edd_cart_discount_<?php echo esc_html($discount);?>" >
    <td class="edd_cart_fee_label">
      <i class="fa fa-percent"></i>
      <span class="edd_checkout_cart_item_title">
        <?php echo __('discount code', 'kiwi').' ('.esc_html($discount).')'; ?>
      </span>
    </td>
    <td class="edd_cart_fee_amount">
      <?php echo esc_html($rate); ?>
    </td>
    <td>
      <a href="<?php echo $remove_url;?>" data-code="<?php echo $discount;?>" class="edd_discount_remove"><?php _e( 'Remove', 'easy-digital-downloads' ); ?></a>
    </td>
  </tr>
  <?php
  return ob_get_clean();
}

function takexpert_custom_discount_points_template(){
  $total_points_discounts = 0;
  foreach( edd_get_cart_fees() as $fee_id => $fee ){
    if($fee['amount'] < 0){
      $total_points_discounts += $fee['amount'];
    }
  }
  $total_points_discounts *= -1;
  if($total_points_discounts > 0){
    $removfeesurl = add_query_arg( array( 'edd_points_remove_discount' => __( 'Remove', 'easy-digital-downloads' ) ), edd_get_current_page_url() );
  ?>
    <tr class="edd_cart_footer_row edd_cart_points_row" id="edd_cart_discount_points">
      <td class="edd_cart_fee_label">
        <i class="fa fa-percent"></i>
        <span class="edd_checkout_cart_item_title">
          <?php echo esc_html( __('Points Discount', 'eddpoints') ); ?>
        </span>
      </td>
      <td class="edd_cart_fee_amount">
        <?php echo esc_html( edd_currency_filter( edd_format_amount( $total_points_discounts ) ) ); ?>
      </td>
      <td>
        <a href="<?php echo $removfeesurl;?>" data-code="discount_points" class="edd_discount_remove" id="edd_points_remove_discount">
          <?php _e( 'Remove', 'eddpoints' ); ?>
        </a>
      </td>
    </tr>
  <?php
  }
}

add_filter( 'edd_customer_dashboard_menu_links', 'takexpeert_custom_edd_customer_menu', 999, 1);
function takexpeert_custom_edd_customer_menu($menu){
  $menu = array();

  $menu[ 'profile' ] = array(
    'task' => 'profile',
    'name' => __( 'Profile', 'kiwi' )
  );

  $menu[ 'purchases' ] = array(
    'task' => 'purchases',
    'name' => __( 'Purchase History', 'kiwi' )
  );

  $menu[ 'downloads' ] = array(
    'task' => 'download',
    'name' => __( 'Purchases', 'kiwi' )
  );

  // Support for EDD Wishlists
  if( class_exists( 'EDD_Wish_Lists' ) ) {
    $menu[ 'wishlists' ] = array(
      'task'=> 'wishlist',
      'name' => __( 'Wishlists', 'kiwi' )
    );
  }
  return $menu;
}

// apply readonly to gravity forms input fields
// update '1' to the ID of your form
add_filter( 'gform_pre_render_35', 'add_readonly_script' );
add_filter( 'gform_pre_render_38', 'add_readonly_script' );
function add_readonly_script( $form ) {
    ?>
    <script type="text/javascript">
        jQuery(document).ready(function(){
            /* apply only to a input with a class of gf_readonly */
            jQuery("li.gf_readonly input").attr("readonly","readonly");
        });
    </script>
    <?php
    return $form;
}


// assign loan to user
function assign_loan_to_user($user_id, $amount, $description){

  $edd_points_admin = new EDD_Points_Admin();

  //get user points label
  $post_data = array(
              'post_title'	=>	sprintf('کسب امتیاز بابت «%s»', $description ),
              'post_content'	=>	sprintf('کسب امتیاز بابت «%s»', $description ),
              'post_author'	=>	$user_id
            );
  $log_meta = array(
              'userpoint'		=>	abs($amount),
              'events'		=>	'manual',
              'operation'		=>	'add' //add or minus
            );

  //insert entry in log
  $edd_points_admin->logs->edd_points_insert_logs( $post_data, $log_meta );

  //update user points
  edd_points_add_points_to_user( $amount, $user_id );
}

?>

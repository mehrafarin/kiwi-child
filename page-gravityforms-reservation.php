<?php
/*
Template Name: GravityForms Reservation
*/

get_header();

// retrieve query strings
if(isset($_REQUEST['first_name']) && !empty($_REQUEST['first_name'])){
	$first_name = htmlspecialchars($_REQUEST['first_name']);
	$template = '[vc_row][vc_column width="1/6"][/vc_column][vc_column width="2/3"][vc_message message_box_color="success" icon_fontawesome="fa fa-check" css_animation="flipInX"]
<p style="text-align: center;"><span style="font-size: 12pt; color: #993366;">'.$first_name.' عزیز رزرو شما با موفقیت انجام شد.</span>
<span style="font-size: 12pt; color: #993366;">موفق باشيد</span></p>
[/vc_message][vc_btn title="بازگشت به صفحه اصلی" color="danger" align="center" i_align="right" i_icon_fontawesome="fa fa-home" css_animation="none" add_icon="true" link="url:%2F|||"][/vc_column][vc_column width="1/6"][/vc_column][/vc_row]';
}
else{
	$template = '[vc_row][vc_column width="1/6"][/vc_column][vc_column width="2/3"][vc_message message_box_color="warning" icon_fontawesome="fa fa-exclamation-triangle" css_animation="flipInX"]
<p style="text-align: center;"><span style="font-size: 12pt; color: #993366;">خطایی رخ داده است، لطفا از طریق بخش تماس با ما، موضوع را با مدیر سایت در میان بگذارید.</span></p>[/vc_message][vc_row_inner][vc_column_inner width="1/2"][vc_btn title="بازگشت به صفحه اصلی" color="danger" align="center" i_align="right" i_icon_fontawesome="fa fa-home" css_animation="none" add_icon="true" link="url:%2F|||"][/vc_column_inner][vc_column_inner width="1/2"][vc_btn title="تماس با ما" color="danger" align="center" i_align="right" i_icon_fontawesome="fa fa-phone-square" css_animation="none" add_icon="true" link="url:%2F%3Fp%3D427||target:%20_blank|"][/vc_column_inner][/vc_row_inner][/vc_column][vc_column width="1/6"][/vc_column][/vc_row]';
}

?>
<div class="container" id="payment-status-template" itemscope itemtype="http://schema.org/Blog">
	<div class="row" role="main">
		<div class="col-sm-12 col-md-12 fullwidth">
		<?php echo do_shortcode($template);?>
		</div>
	</div>
</div>

<?php get_footer(); ?>

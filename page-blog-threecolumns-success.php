<?php
/*
Template Name: Blog Posts 3 Columns Success only
*/

get_header();

global $kiwi_theme_option;
?>

		<div class="container" id="success-template" itemscope itemtype="http://schema.org/Blog">
        		    <div class="success-panel">
                                        <?php echo do_shortcode('[vc_row][vc_column][vc_btn title="ثبت موفقيت در سايت" color="danger" link="url:%2F%3Fp%3D25705|title:%D8%AB%D8%A8%D8%AA%20%D9%85%D9%88%D9%81%D9%82%D9%8A%D8%AA%20%D8%AF%D8%B1%20%D8%B3%D8%A7%D9%8A%D8%AA|target:%20_blank|" el_id="add-new-success" el_class="add-new-success-btn"][/vc_column][/vc_row]'); ?>
                                    </div>
			<div class="row" role="main">
		    <?php if ( $kiwi_theme_option['sidebar-blog-enable'] == '0' ) { ?>
				<div class="col-sm-12 col-md-12 fullwidth">

			<?php } ?>


		<?php if ( have_posts() ) : ?>
			<div class="template-page">

			<?php if ( $kiwi_theme_option['page-pagetitle-enable'] == '1' ) { ?>
				<h3 class="index"><?php echo wp_get_document_title(); ?></h3>
			<?php } ?>

			</div>

				<div class="grid-masonry three-columns index-layout category">

			<?php
				$postcount = $kiwi_theme_option['sidebar-blog-postcount'];
				$wp_query->query('posts_per_page='.$postcount.'&category__in=298' . '&paged='.$paged);


				while ($wp_query->have_posts()) : $wp_query->the_post();
			?>

			<?php get_template_part( 'content', get_post_format() ); ?>

			<?php endwhile; ?>

				</div>


			<div class="clear"></div>

			<?php kiwi_content_nav( 'nav-below' ); ?>

		<?php else : ?>
			<?php get_template_part( 'templates/content', 'none' ); ?>
		<?php endif; ?>

			</div>


	</div>
</div>

<?php get_footer(); ?>

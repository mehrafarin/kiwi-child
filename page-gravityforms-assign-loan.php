<?php
/*
Template Name: GravityForms assign Loan
*/

get_header();

if(!is_user_logged_in()){
	$template = '[vc_row][vc_column width="1/6"][/vc_column][vc_column width="2/3"][vc_column_text]<div class="required-login"><div class="content"><p style="color:#800080">برای استفاده از این بخش به سایت وارد شوید <br>چناچه قبلا عضو نشده اید، ابتدا در سایت عضو شوید</p></div><br><div class="help-buttons"> <a class="button takexpert-dynamic-button" href="https://azamsadeghian.com/?p=350">ورود</a> <a class="button takexpert-dynamic-button" href="https://azamsadeghian.com/?p=7994">عضویت</a> <a class="faq button takexpert-dynamic-button" href="https://azamsadeghian.com/?p=24011" target="_blank">راهنمای عضویت</a></div></div>[/vc_column_text][/vc_column][vc_column width="1/6"][/vc_column]';
} else{
	// retrieve query strings
	if(!isset($_REQUEST['desc']) ||	empty($_REQUEST['desc'])){
		$template = '[vc_row][vc_column width="1/6"][/vc_column][vc_column width="2/3"][vc_message message_box_color="warning" icon_fontawesome="fa fa-exclamation-triangle" css_animation="flipInX"]
	<p style="text-align: center;"><span style="font-size: 12pt; color: #993366;">خطایی رخ داده است، لطفا از طریق بخش تماس با ما، موضوع را با مدیر سایت در میان بگذارید.</span></p>[/vc_message][vc_row_inner][vc_column_inner width="1/2"][vc_btn title="بازگشت به صفحه اصلی" color="danger" align="center" i_align="right" i_icon_fontawesome="fa fa-home" css_animation="none" add_icon="true" link="url:%2F|||"][/vc_column_inner][vc_column_inner width="1/2"][vc_btn title="تماس با ما" color="danger" align="center" i_align="right" i_icon_fontawesome="fa fa-phone-square" css_animation="none" add_icon="true" link="url:%2F%3Fp%3D427||target:%20_blank|"][/vc_column_inner][/vc_row_inner][/vc_column][vc_column width="1/6"][/vc_column][/vc_row]';
	}	else{
		$desc = htmlspecialchars($_REQUEST['desc']);
		$template = '[vc_row][vc_column width="1/6"][/vc_column][vc_column width="2/3"][vc_message message_box_color="success" icon_fontawesome="fa fa-check" css_animation="flipInX"]
<p style="direction: rtl; text-align: center;"><span style="color: #993366;"><span style="font-size: 16px;">درخواست شما ثبت شد و پس از بررسی، مدیرسایت با شما در ارتباط خواهد بود و دسترسی شما رو به '.$desc.' باز خواهد کرد.</span></span></p>
<p style="direction: rtl; text-align: center;"><span style="color: #993366;"><span style="font-size: 16px;">لطفا صبور باشید.</span></span></p>
[/vc_message][vc_row_inner][vc_column_inner][vc_btn title="بازگشت به صفحه اصلی" color="danger" align="center" i_align="right" i_icon_fontawesome="fa fa-home" css_animation="none" add_icon="true" link="url:%2F|||"][/vc_column_inner][/vc_row_inner][/vc_column][vc_column width="1/6"][/vc_column][/vc_row]';
	}
}

?>
<div class="container" id="payment-status-template" itemscope itemtype="http://schema.org/Blog">
	<div class="row" role="main">
		<div class="col-sm-12 col-md-12 fullwidth">
		<?php echo do_shortcode($template);?>
		</div>
	</div>
</div>

<?php get_footer(); ?>

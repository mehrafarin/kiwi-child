<?php
/*
Template Name: Free Download
*/

get_header();

global $kiwi_theme_option;
?>
<div class="container" id="success-template" itemscope itemtype="http://schema.org/Blog">
	<div class="row" role="main">
			<?php if ( $kiwi_theme_option['sidebar-blog-enable'] == '0' ) { ?>
		<div class="col-sm-12 col-md-12 fullwidth">

		<?php } ?>


	<?php if ( have_posts() ) : ?>
			<div class="template-page">

		<?php if ( $kiwi_theme_option['page-pagetitle-enable'] == '1' ) { ?>
				<h3 class="index"><?php echo wp_get_document_title(); ?></h3>
		<?php } ?>

			</div>

			<div class="grid-masonry three-columns index-layout category">

		<?php
			$postcount = $kiwi_theme_option['sidebar-blog-postcount'];
			$wp_query->query(array('posts_per_page' => $postcount,
			'cat' => 423,
			'paged' => $paged));


			while ($wp_query->have_posts()) : $wp_query->the_post();
		?>

		<?php get_template_part( 'content', get_post_format() ); ?>

		<?php endwhile; ?>

			</div>


		<div class="clear"></div>

		<?php kiwi_content_nav( 'nav-below' ); ?>

	<?php else : ?>
		<?php get_template_part( 'templates/content', 'none' ); ?>
	<?php endif; ?>
		</div>
	</div>
</div>

<?php get_footer(); ?>

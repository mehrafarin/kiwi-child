<?php
/* This template is used to display the profile editor with [edd_profile_editor] */
global $current_user;

if ( is_user_logged_in() ):
	$user_id      = get_current_user_id();
	$user_object = get_userdata( $user_id );
	$profile_url = $user_object->user_url;


	if ( edd_is_cart_saved() ): ?>
		<?php $restore_url = add_query_arg( array( 'edd_action' => 'restore_cart', 'edd_cart_token' => edd_get_cart_token() ), edd_get_checkout_uri() ); ?>
		<div class="edd_success edd-alert edd-alert-success"><strong><?php esc_html_e( 'Saved cart', 'kiwi'); ?>:</strong> <?php printf( esc_html__( 'You have a saved cart, <a href="%s">click here</a> to restore it.', 'kiwi' ), esc_url( $restore_url ) ); ?></div>
	<?php endif; ?>

	<?php if ( isset( $_GET['updated'] ) && $_GET['updated'] == true && ! edd_get_errors() ): ?>
		<div class="edd_success edd-alert edd-alert-success"><strong><?php esc_html_e( 'Success', 'kiwi'); ?>:</strong> <?php esc_html_e( 'Your profile has been edited successfully.', 'kiwi' ); ?></div>
	<?php endif; ?>

	<?php edd_print_errors(); ?>

	<?php do_action( 'edd_profile_editor_before' ); ?>

	<form id="edd_profile_editor_form" class="edd_form" action="<?php echo edd_get_current_page_url(); ?>" method="post">
		<fieldset>
			<span id="edd_profile_name_label">

			<div class="edd_widget_title"><h3><?php esc_html_e( 'Change your Email', 'kiwi' ); ?></h3></div>
			<p>
				<label for="edd_email"><?php esc_html_e( 'Email Address', 'kiwi' ); ?> (اختیاری)</label>
				<input name="edd_email" id="edd_email" class="text edd-input required" type="email" value="<?php echo esc_attr( $current_user->user_email ); ?>" />
				<?php do_action( 'edd_profile_editor_email' ); ?>
			</p>
			<p class="edd_password_change_notice"><?php esc_html_e( 'Please change your email if only you have or needed to receive emails', 'kiwi' ); ?></p>
			<?php do_action( 'edd_profile_editor_after_email' ); ?>

			<div class="edd_widget_title"><h3><?php esc_html_e( 'Change display name', 'kiwi' ); ?></h3></div>
			<p id="edd_profile_display_name_wrap">
				<label for="edd_display_name"><?php _e( 'Display Name', 'easy-digital-downloads' ); ?></label>
				<select name="edd_display_name" id="edd_display_name" class="select edd-select">
					<?php if ( ! empty( $current_user->first_name ) ): ?>
					<option <?php selected( $display_name, $current_user->first_name ); ?> value="<?php echo esc_attr( $current_user->first_name ); ?>"><?php echo esc_html( $current_user->first_name ); ?></option>
					<?php endif; ?>
					<option <?php selected( $display_name, $current_user->user_nicename ); ?> value="<?php echo esc_attr( $current_user->user_nicename ); ?>"><?php echo esc_html( $current_user->user_nicename ); ?></option>
					<?php if ( ! empty( $current_user->last_name ) ): ?>
					<option <?php selected( $display_name, $current_user->last_name ); ?> value="<?php echo esc_attr( $current_user->last_name ); ?>"><?php echo esc_html( $current_user->last_name ); ?></option>
					<?php endif; ?>
					<?php if ( ! empty( $current_user->first_name ) && ! empty( $current_user->last_name ) ): ?>
					<option <?php selected( $display_name, $current_user->first_name . ' ' . $current_user->last_name ); ?> value="<?php echo esc_attr( $current_user->first_name . ' ' . $current_user->last_name ); ?>"><?php echo esc_html( $current_user->first_name . ' ' . $current_user->last_name ); ?></option>
					<option <?php selected( $display_name, $current_user->last_name . ' ' . $current_user->first_name ); ?> value="<?php echo esc_attr( $current_user->last_name . ' ' . $current_user->first_name ); ?>"><?php echo esc_html( $current_user->last_name . ' ' . $current_user->first_name ); ?></option>
					<?php endif; ?>
				</select>
				<?php do_action( 'edd_profile_editor_name' ); ?>
			</p>
			<p class="edd_password_change_notice"><?php esc_html_e( 'display name description', 'kiwi' ); ?></p>

			<div class="edd_widget_title"><h3><?php esc_html_e( 'Change your Password', 'kiwi' ); ?></h3></div>

			<p id="edd_profile_password_wrap">
				<label for="edd_user_pass"><?php esc_html_e( 'New Password', 'kiwi' ); ?></label>
				<input name="edd_new_user_pass1" id="edd_new_user_pass1" class="password edd-input" type="password"/>
				<br />
				<label for="edd_user_pass"><?php esc_html_e( 'Re-enter Password', 'kiwi' ); ?></label>
				<input name="edd_new_user_pass2" id="edd_new_user_pass2" class="password edd-input" type="password"/>
				<?php do_action( 'edd_profile_editor_password' ); ?>
			</p>

			<p class="edd_password_change_notice"><?php esc_html_e( 'Please note after changing your password, you must log back in.', 'kiwi' ); ?></p>
			<?php do_action( 'edd_profile_editor_after_password' ); ?>


<br />
	<p id="edd_profile_submit_wrap">
		<input type="hidden" name="edd_profile_editor_nonce" value="<?php echo wp_create_nonce( 'edd-profile-editor-nonce' ); ?>"/>
		<input type="hidden" name="edd_action" value="edit_user_profile" />
		<input type="hidden" name="edd_redirect" value="<?php echo esc_url( edd_get_current_page_url() ); ?>" />
		<input name="edd_profile_editor_submit" id="edd_profile_editor_submit" type="submit" class="edd_submit" value="<?php esc_html_e( 'Save Changes', 'kiwi' ); ?>"/>
	</p>



		</fieldset>
	</form><!-- #edd_profile_editor_form -->

	<?php do_action( 'edd_profile_editor_after' ); ?>

	<?php
else:
	echo esc_html__( 'You need to login to edit your profile.', 'kiwi' );
	echo edd_login_form();
endif;

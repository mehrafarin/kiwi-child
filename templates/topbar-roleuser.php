<?php

global $kiwi_theme_option;

	$current_user = wp_get_current_user();

	$author_id = $current_user->ID;
    $registered = date_i18n( get_option('date_format'), strtotime( get_the_author_meta( 'user_registered', $author_id ) ) );
	$vendor_name = $current_user->user_login;

	if( class_exists( 'EDD_Wallet' ) ) {
		$value = edd_wallet()->wallet->balance( $current_user->ID );
		$value = edd_currency_filter( edd_format_amount( $value ) );
	}

	$vendorname = '<span class="vendor-name">'. esc_html( $current_user->display_name ).'</span>';

	$url_id = $kiwi_theme_option['marketplace-enablelinks-dashboard-subscriber'];
	$buyer_link = get_page_link ($url_id);


	if( class_exists( 'EDD_Wallet' ) ) {

		$url_id_wallet = $kiwi_theme_option['marketplace-userrole-wallet'];
		$wallet_link = get_page_link ($url_id_wallet);

		$li_wallet = '<li class="menu-item edd-wallet credits"><a href="'.esc_url( $wallet_link ).'">'. esc_html__( 'Make a deposit', 'kiwi' ).'</a></li>';
		$show_wallet = '<span class="sep">'. esc_html__( ' | ', 'kiwi' ).'</span><span class="wallet-value">'. esc_html( $value ) .'</span>';
	} else {
		$li_wallet = '';
		$show_wallet = '';
	}

	if ( class_exists( 'EDD_Front_End_Submissions' ) && EDD_FES()->vendors->vendor_is_vendor() ) {
		$dashboard_nonvendor = '';
		$url = get_permalink( EDD_FES()->helper->get_option( 'fes-vendor-dashboard-page', get_permalink() ) );
		$constant = EDD_FES()->helper->get_option( 'fes-vendor-constant', '' );
		$constant = ( isset( $constant ) && $constant != '' ) ? $constant : esc_html__( 'vendor', 'kiwi' );
		$constant = apply_filters( 'fes_vendor_constant_singular_lowercase', $constant );

		$show_vendorpage = '<li class="menu-item vendorpage"><a href="'.esc_url(site_url().'/'.$constant.'/'.str_replace(" ","-", $vendor_name)).'">'. esc_html__( 'Vendor page', 'kiwi' ).'</a></li>';
	} elseif ( class_exists( 'EDD_Front_End_Submissions' ) && !EDD_FES()->vendors->vendor_is_vendor() ) {
		$dashboard_nonvendor = '<li class="menu-item customer-dashboard"><a href="'. esc_url( $buyer_link ).'">'. esc_html__( 'Dashboard', 'kiwi' ).'</a></li>';
		$url_id = $kiwi_theme_option['marketplace-enablelinks-dashboard-subscriber'] ;
		$url = get_page_link ($url_id);
		$show_vendorpage = '<li class="menu-item becomevendor"><a href="'. esc_url( add_query_arg( 'task', 'fes_become_vendor', $buyer_link ) ).'">'. esc_html__( 'Become a vendor', 'kiwi' ).'</a></li>';
	}	else {
		$url_id = $kiwi_theme_option['marketplace-enablelinks-dashboard-subscriber'] ;
		$url = get_page_link ($url_id);

		$show_vendorpage = '';
		// $dashboard_nonvendor = '<li class="menu-item customer-dashboard"><a href="'. esc_url( $buyer_link ).'">'. esc_html__( 'Dashboard', 'kiwi' ).'</a></li>';
	}


	if (class_exists('EDD_Points_Public') ) {
		$point_reward = '<span class="points-rewards">'. sprintf( esc_html__( 'Points: %s', 'kiwi' ), esc_attr( edd_points_get_user_points() ) ).'</span>';

		/* $reward = edd_points_get_user_points();

		if( ! empty( $reward ) ) {
		 */	$extra_class= ' points';
		/* } else {
			$extra_class= '';
		} */

	} else {
		$point_reward = '';
		$extra_class= '';
	}


?>

<li class="menu-item menu-item-has-children pro-vendor"><a href="#"><?php echo $vendorname; ?><?php echo $show_wallet; ?></a>
	<ul class="dropdown-menu">
		<li class="heading">
			<div class="avatar"><?php echo get_avatar( $current_user->ID , 50); ?></div>
			<div class="info<?php echo esc_attr( $extra_class ); ?>">
				<?php echo $vendorname; ?>
				<span class="vendormemberdate"><?php echo esc_html( $registered ); ?></span>
				<?php echo $point_reward; ?>
			</div>
			<div class="clear"></div>
		</li>
		<?php if(isset($dashboard_nonvendor)) echo $dashboard_nonvendor; ?>
		<li class="menu-item profile"><a href="<?php echo esc_url( add_query_arg( 'task', 'profile', $url ) ); ?>"><?php esc_html_e( 'Profile', 'kiwi' ); ?></a></li>
		<li class="menu-item purchases"><a href="<?php echo esc_url( add_query_arg( 'task', 'download', $url ) ); ?>"><?php esc_html_e( 'Purchases', 'kiwi' ); ?></a></li>

		<?php if( class_exists( 'EDD_Wish_Lists' ) ) { ?>
		<li class="menu-item wishlist"><a href="<?php echo esc_url( add_query_arg( 'task', 'wishlist', $url ) ); ?>"><?php esc_html_e( 'Favorites', 'kiwi' ); ?></a></li>
		<?php } ?>

		<li class="menu-item invoices"><a href="<?php echo esc_url( add_query_arg( 'task', 'purchases', $url ) ); ?>"><?php esc_html_e( 'Invoices', 'kiwi' ); ?></a></li>
		<?php echo $show_vendorpage; ?>
		<?php echo $li_wallet; ?>
		<li class="menu-item mp-logout"><a href="<?php echo esc_url( wp_logout_url( home_url() ) ); ?>"><?php esc_html_e( 'Log out', 'kiwi' ); ?></a></li>
	</ul>
</li>

<?php global $kiwi_theme_option, $post; 

   	$mp_demo_url	 = esc_url( get_post_meta( get_the_ID(), '_cmb2_marketplace_demo_url', true ) );	
	$mp_images_files = get_post_meta( get_the_ID(), '_cmb2_mp_output_item_images', true );

	if (class_exists( 'EDD_Front_End_Submissions' ) ){ 
		$fes_demo_url = get_post_meta( get_the_ID(), 'demo_url', true );
		$vendor_previews = get_post_meta( get_the_ID(), 'upload_item_previews', true );		
	} 	
	
?>


	<?php if (class_exists('EDD_Hide_Download')) { ?>
		<?php global $wp_query;
				$id = $wp_query->get_queried_object_id();
				$checked = get_post_meta( $id, '_edd_hide_download', true );
		?>		
	<?php } ?>
	
	
		<?php if (class_exists('EDD_Hide_Download') && isset( $checked ) && !empty( $checked ) ) { ?>				
					<div class="mp-hide-notice">
						<?php esc_html_e('This item is currently under review and only visible to you.', 'kiwi'); ?>   
					</div>					
		<?php } ?>	
		
		
		<?php do_action( 'mp_item_desc_previously_purchased' ); ?>
			
		
		
		<?php if ( $kiwi_theme_option['marketplace-enable-design'] == '1') { 
					echo '<div class="product-description row padding40-bottom">';
			    } else {
					echo '<div class="product-description row designtwo padding40-bottom">';
		} ?>
		
		
		
	<?php if ( $kiwi_theme_option['marketplace-enable-design'] == '1') { ?>	
		  <div class="col-md-2">							
					
			<div class="mp-social-buttons">	
			
			<?php do_action( 'mp_item_desc_socialmedia_buttons' ); ?>						
			
				<?php if ( class_exists ( 'ESSB_Manager' )) {					
					
					$url = get_permalink($post->ID);
					$title = get_the_title($post->ID);

					 echo do_shortcode('[easy-social-share counters=1 counter_pos="bottom" total_counter_pos="none" template="grey-blocks-retina" url="'.$url.'" title="'.$title.'"]');
					 echo '<div class="clear"></div>';
				} 
					
				?>
			
			<?php if ( is_plugin_active( 'edd-coming-soon/edd-coming-soon.php' ) ) { ?>
				<?php if ( edd_coming_soon_is_active( get_the_ID() ) ) { ?>									
				
				<?php } ?>														
			<?php } ?>	
				
	
			<?php if (!class_exists('EDD_Front_End_Submissions') && !empty( $mp_demo_url) || class_exists( 'EDD_Front_End_Submissions' ) && empty( $fes_demo_url ) && !empty( $mp_demo_url ) ){ ?>			
				<div class="demo"><a href="<?php echo esc_url( $mp_demo_url ); ?>" target="_blank"><i class="fa fa-globe"></i> <span><?php esc_html_e('Live demo', 'kiwi'); ?></span></a></div>					
			<?php } ?>
			
			
			<?php if (class_exists( 'EDD_Front_End_Submissions' ) && !empty( $fes_demo_url )) { ?>	
				<div class="demo"><a href="<?php echo esc_url( $fes_demo_url ); ?>" target="_blank"><i class="fa fa-globe"></i> <span><?php esc_html_e('Live demo', 'kiwi'); ?></span></a></div>					
			<?php } ?>
								
		 </div>	
			
						  
	  </div>
	<?php } ?>
		  
		  
		   <?php if ( $kiwi_theme_option['marketplace-enable-design'] == '1') { 
					echo '<div class="col-md-12 thumb-preview">';
				} else {
					echo '<div class="col-md-12 thumb-preview">';
			    } 
			   
				if ( $kiwi_theme_option['marketplace-enable-design'] == '2') { 
					$full = ' full-width';
			    } else {
					$full = '';
				}
			
			?>
		  
			<div>
			
		
		<?php if( function_exists('mp_audio_video_featured')){ mp_audio_video_featured(); } ?>

				
				<?php 
					
					do_action( 'mp_item_desc_slider_thumbs' );	

					if ( $kiwi_theme_option['miscellaneous-vc-rtl-support'] == '1' ) {
						$direction = 'rtl';
					} else {
						$direction = 'ltr';
					}
										
					if ( class_exists( 'EDD_Front_End_Submissions' ) && !empty( $vendor_previews ) ) {
						echo '<div class="mp-display-image'. esc_attr( $full ).' flex-edd-images"><ul class="slides none" dir="'.esc_attr( $direction ).'">';		
							foreach($vendor_previews as $key=>$val){
							   $url = wp_get_attachment_image_src( $val, 'full' );
							   $url_thumb = wp_get_attachment_image_src( $val, 'mp-slider-product-page' );
								if(!empty($kiwi_theme_option['mp-lightbox'] )) {	
									echo '<li><a href="'. esc_url( $url[0] ).'" '.$kiwi_theme_option['mp-lightbox'].'><img src="'. esc_url( $url_thumb[0] ).'"></a></li>';
								} else {
									echo '<li><a href="'. esc_url( $url[0] ) .'"><img src="'. esc_url( $url_thumb[0] ).'"></a></li>';
								}
							}		
						echo '</ul>';
						
						echo '<ul class="flex-direction-nav">
								<li><a href="#" class="flex-prev"></a></li>
								<li><a href="#" class="flex-next"></a></li>
							</ul>';
							
						echo '</div>';
						
					} elseif (!class_exists( 'EDD_Front_End_Submissions' ) && !empty( $mp_images_files ) || class_exists( 'EDD_Front_End_Submissions' ) && empty( $vendor_previews ) && !empty( $mp_images_files ) && function_exists( 'cmb2_output_file_list' ) ) {				
							echo cmb2_output_file_list( '_cmb2_mp_output_item_images', 'small' );
					} else {
							echo '';
					}
					
				?> 
				
			</div>							
		  </div>
		</div>	
		<div class="clear"></div>	
		
		
		<?php if ( comments_open() || '0' != get_comments_number() ) {
				$class_comment = ' comments-open';
			} else {
				$class_comment = ' comments-closed';
			}
		?>
		

	<?php if (!empty_content($post->post_content)) { ?>			
		<div class="marketplace-standard full-description<?php echo $class_comment; ?>">	
				<div class="support-category">
					<h4 class="mpdesc"><?php esc_html_e( 'Item description', 'kiwi' ); ?></h4>	
				
				<!-- <?php if ( class_exists( 'EDD_Reviews' ) ) { mp_extension_review_function_colors(); } ?> -->
				
				</div>					
				
				<?php
					//if ( has_filter( 'the_content', 'edd_coming_soon_single_download' ) )
					remove_action( 'the_content', 'microdata', 9999 );
					remove_filter( 'the_content', 'edd_coming_soon_single_download' );					
					remove_filter( 'edd_coming_soon_display_text', 'edd_coming_soon_get_custom_status_text', 30, 2 );					
					remove_filter( 'the_content', 'edd_csau_single_download_upsells', 100 );	
					remove_filter( 'edd_after_download_content', 'edd_rp_display_single', 10, 1 );	
				?>	
				
				<?php the_content(); ?>					
		</div>	
	<?php } ?>
	
		
	
	<?php if( class_exists( 'EDD_Upload_File' ) ) { mp_cross_sell_items(); } ?>	
	<?php if( class_exists( 'EDDRecommendedDownloads' ) ) {	mp_edd_ext_recommened_items(); } ?>
		
	<?php if ( class_exists( 'EDD_Reviews' ) ) {  mp_extension_reviews_content(); } ?>
		
		
		
	
	<?php if (empty_content($post->post_content)) { 
			$div_class = ' no-mp-content';
		} else {
			$div_class = '';
		}
	?>
	
	<?php if ( $kiwi_theme_option['marketplace-enable-comments'] == '1') { ?>		
			<?php if ( comments_open() || '0' != get_comments_number() ) comments_template( '', true ); ?>		
	<?php } ?>

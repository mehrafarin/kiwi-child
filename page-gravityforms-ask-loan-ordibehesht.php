<style type="text/css">
.vc_message_box-icon{display: none;}
.vc_message_box ul{margin: 20px !important;}
.vc_message_box {padding: 1em 1em 1em 1em !important;}
.purple-border{border: #80007F 2px solid;}
.hidden-row { display: none!important;}
</style>
<?php
/*
Template Name: GravityForms ask Loan ordibehesht
*/


get_header();

if(is_user_logged_in()){
	$data = '[gravityform id="35" title="false" description="false" ajax="true"]';
	$data2 = '[vc_message color="warning" message_box_color="warning" icon_fontawesome="fa fa-exclamation-triangle"]
	<p style="text-align: justify;"><span style="font-size: 12pt; color: #ff0000;">توجه داشته باشید که برای بهره مندی از کمک هزینه حتما حتما باید با اکانت خودتان وارد سایت شوید. این کمک هزینه فقط برای شخصی قابل استفاده خواهد بود که نام و نام خانوادگی و شماره موبایل او در فرم درخواست باشد.</span></p>
<p style="text-align: justify;"><span style="font-size: 12pt; color: #993366;">فرم زیر درخواست کمک هزینه برای اردیبهشت ماه محصول «باشگاه بهترین سال زندگی» می باشد و مجاز به استفاده از آن برای سایر محصولات یا خدمات سایت نخواهید بود.</span></p>
[/vc_message][vc_message color="danger" message_box_color="danger" icon_fontawesome="fa fa-times"]
	<p style="text-align: justify;"><span style="font-size: 12pt; color: #ff0000;">اطلاعات زیر قابل تغییر نمی باشند، چنانچه اطلاعات زیر متعلق به شخص دیگری است، لازم است که از اکانت آن شخص خارج شوید و سپس با اکانت خود به سایت وارد شوید.</span></p>
	[/vc_message][vc_btn title="خروج از سایت" color="danger" align="center" i_icon_fontawesome="fa fa-sign-out" add_icon="true" link="url:'. urlencode(wp_logout_url('/?p='.$post->ID))
	.'|title:%D8%AE%D8%B1%D9%88%D8%AC%20%D8%A7%D8%B2%20%D8%B3%D8%A7%DB%8C%D8%AA||"]';
} else {
	$data = '[vc_column_text]<div class="required-login"><div class="content"><p style="color:#800080">برای استفاده از این بخش به سایت وارد شوید <br>چناچه قبلا عضو نشده اید، ابتدا در سایت عضو شوید</p></div><br><div class="help-buttons"> <a class="button takexpert-dynamic-button" href="https://azamsadeghian.com/?p=350">ورود</a> <a class="button takexpert-dynamic-button" href="https://azamsadeghian.com/?p=7994">عضویت</a> <a class="faq button takexpert-dynamic-button" href="https://azamsadeghian.com/?p=24011" target="_blank">راهنمای عضویت</a></div></div>[/vc_column_text]';
	$data2 = '';
}
	$template = '[vc_row][vc_column width="1/4"][/vc_column][vc_column width="1/2" el_class="purple-border" css=".vc_custom_1586195822788{background-color: #ffffff !important;}"]'.$data2.'[vc_row_inner css=".vc_custom_1587753935792{background-color: #fff1b5 !important;}" el_class="gf-section"][vc_column_inner]'.$data.'[/vc_column_inner][/vc_row_inner][/vc_column][vc_column width="1/4"][/vc_column][/vc_row][vc_row][vc_column][vc_column_text][/vc_column_text][/vc_column][/vc_row]';
?>
<div class="container" id="payment-status-template" itemscope itemtype="http://schema.org/Blog">
	<div class="row" role="main">
		<div class="col-sm-12 col-md-12 fullwidth">
		<?php echo do_shortcode($template);?>
		</div>
	</div>
</div>

<?php get_footer(); ?>
